<?php
    
    /**
     Plugin Name: Prometheus Metrics in Wordpress
     Plugin URI: https://gitlab.com/pheidotting/wp-prometheus
     Description: Toon Prometheus Metrics vanuit Wordpress, te weten: Databasesize, aantal bezoekers van vandaag uit Matomo (indien geactiveerd), aantal updates beschikbaar voor Wordpress zelf, plugins en themes, aantal producten en aantal orders uit WooCommerce (indien geactiveerd)
     Version: 1.2
     Author: Patrick Heidotting
     Author URI: http://heidotting.nl/
     License: GPL2
     */
    
    add_filter( 'rest_pre_serve_request', 'prometheus_serve_request', 10, 4 );
    add_action( 'rest_api_init', 'prometheus_register_route' );
    
    function prometheus_get_metrics() {
        global $wpdb, $table_prefix;
        
        $result = '';
        
        // Database size
        $query   = $wpdb->get_results( "SELECT SUM(ROUND(((data_length + index_length) / 1024 / 1024), 2)) as value FROM information_schema.TABLES WHERE table_schema = '" . DB_NAME . "'", ARRAY_A ); // phpcs:ignore WordPress.DB
        $result .= "# HELP wp_db_size Total DB size in MB.\n";
        $result .= "# TYPE wp_db_size counter\n";
        $result .= 'wp_db_size{host="' . get_site_url() . '"} ' . $query[ 0 ][ 'value' ] . "\n";
        
        //Bezoekers vandaag
        if(is_plugin_active("matomo/matomo.php")){
            $query = $wpdb->get_results("SELECT * FROM `" . $table_prefix . "matomo_log_visit` WHERE `visit_last_action_time` BETWEEN '".date("Y-m-d")." 00:00:00.000000' AND '".date("Y-m-d")." 23:59:59.000000'", ARRAY_A);
            $result .= "# HELP Vistors today.\n";
            $result .= "# TYPE wp_visitors_today counter\n";
            $result .= 'wp_visitors_today{host="' . get_site_url() . '"} ' . count( $query ) . "\n";
        }
        
        //Updates
        $updates = core_update_check() ? 1 : 0;
        $updates = $updates + plugins_update_check();
        $updates = $updates + themes_update_check();
        $result .= 'wp_updates_available{host="' . get_site_url() . '"} ' . $updates . "\n";
        
        if(is_plugin_active("woocommerce/woocommerce.php")){
            $query = $wpdb->get_results("SELECT * FROM `" . $table_prefix . "posts` WHERE `post_parent` = 0 AND `post_type` = 'product' AND `post_status` = 'publish'", ARRAY_A);
            $result .= "# HELP aantal producten.\n";
            $result .= "# TYPE wp_number_of_products_published counter\n";
            $result .= 'wp_number_of_products_published{host="' . get_site_url() . '"} ' . count( $query ) . "\n";

            $query = $wpdb->get_results("SELECT * FROM `" . $table_prefix . "posts` WHERE `post_parent` = 0 AND `post_type` = 'shop_order'", ARRAY_A);
            $result .= "# HELP aantal orders.\n";
            $result .= "# TYPE wp_number_of_orders counter\n";
            $result .= 'wp_number_of_orders{host="' . get_site_url() . '"} ' . count( $query ) . "\n";
        }

        /**
         * Filter database metrics result
         *
         * @var string $result The database metrics result
         */
        $result = apply_filters( 'prometheus_custom_metrics', $result );
        
        return $result;
    }
    
    function core_update_check( ) {
        global $wp_version;
        do_action( 'wp_version_check' ); // force WP to check its core for updates
        $update_core = get_site_transient( 'update_core' ); // get information of updates
        if ( 'upgrade' === $update_core->updates[0]->response ) { // is WP core update available?
            if ( $update_core->updates[0]->current !== $settings['notified']['core'] ) { // have we already notified about this version?
                require_once ABSPATH . WPINC . '/version.php'; // Including this because some plugins can mess with the real version stored in the DB.
                $new_core_ver                 = $update_core->updates[0]->current; // The new WP core version
                $old_core_ver                 = $wp_version; // the old WP core version
                return true; // we have updates so return true
            } else {
                return false; // There are updates but we have already notified in the past.
            }
        }
        return false; // no updates return false
    }
    
    function plugins_update_check( ) {
        global $wp_version;
        $cur_wp_version = preg_replace( '/-.*$/', '', $wp_version );
        do_action( 'wp_update_plugins' ); // force WP to check plugins for updates
        $update_plugins = get_site_transient( 'update_plugins' ); // get information of updates
        if ( ! empty( $update_plugins->response ) ) { // any plugin updates available?
            $plugins_need_update = $update_plugins->response; // plugins that need updating
            if ( count( $plugins_need_update ) >= 1 ) { // any plugins need updating after all the filtering gone on above?
                return count( $plugins_need_update ); // we have plugin updates return true
            } else {
                return 0;
            }
            return 0;
        }
        return 0;
    }
    
    function themes_update_check( ) {
        do_action( 'wp_update_themes' ); // force WP to check for theme updates
        $update_themes = get_site_transient( 'update_themes' ); // get information of updates
        if ( ! empty( $update_themes->response ) ) { // any theme updates available?
            $themes_need_update = $update_themes->response; // themes that need updating
            $themes_need_update = apply_filters( 'sc_wpun_themes_need_update', $themes_need_update ); // additional filtering of themes need update
            if ( count( $themes_need_update ) >= 1 ) { // any themes need updating after all the filtering gone on above?
                return count( $themes_need_update ); // we have theme updates return true
            } else {
                return 0;
            }
            return 0;
        }
        return 0;
    }
    
    function prometheus_empty_func() {
        return '{ "error": "You cannot access to that page" }';
    }
    
    function prometheus_serve_request( $served, $result, $request, $server ) {
        if ( !defined( 'PROMETHEUS_KEY' ) && $request->get_route() === '/metrics' ) {
            echo prometheus_empty_func(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
            $served = true;
        }
        
        if ( isset( $_GET[ 'prometheus' ] ) && esc_html( $_GET[ 'prometheus' ] ) === PROMETHEUS_KEY ) {
            header( 'Content-Type: text/plain; charset=' . get_option( 'blog_charset' ) );
            $metrics = prometheus_get_metrics();
            echo $metrics; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
            $served = true;
        }
        
        return $served;
    }
    
    function prometheus_register_route() {
        register_rest_route(
                            'metrics',
                            '/',
                            array(
                                  'methods'  => 'GET',
                                  'callback' => 'prometheus_empty_func',
                                  )
                            );
    }
